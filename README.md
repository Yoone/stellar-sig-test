stellar-sig-test
================

The goal of this small project was to test the capability of 3 Stellar.org SDKs (Golang, Python, JS) to sign a message using a private key, without the help of external libraries.

Requirements
------------

* `golang`
* `python3` with `virtualenv`
* `node`

Usage
-----

* Setup all: `make`
* Run tests: `make run`
