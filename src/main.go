package main

import (
	"encoding/base64"
    "flag"
	"fmt"

	"github.com/stellar/go/keypair"
)

func main() {
	seed := flag.String("seed", "", "Seed of the account signing")
	input := flag.String("input", "OXIO", "Message to sign")
    flag.Parse()

	kp, err := keypair.Parse(*seed)
	if err != nil {
		panic(err)
	}
	sig, err := kp.Sign([]byte(*input))
	if err != nil {
		panic(err)
	}

	data := base64.StdEncoding.EncodeToString(sig)
	fmt.Println(data)
}
