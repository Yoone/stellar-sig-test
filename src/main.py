#!/usr/bin/env python3

import argparse
import base64

from stellar_base.keypair import Keypair


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--seed', help='Seed of the account signing')
    parser.add_argument('--input', help='Message to sign')
    args = parser.parse_args()

    kp = Keypair.from_seed(args.seed)
    sig = kp.sign(args.input.encode('utf-8'))
    data = base64.b64encode(sig)
    print(data.decode('utf-8'))
