const Stellar = require('stellar-base')

const parseArgs = (wanted) => {
    const args = {}
    wanted.forEach((val, index, array) => {
        argIdx = process.argv.indexOf(val)
        if (argIdx === -1 || argIdx + 1 === process.argv.length) {
            console.error('Error: ' + val + ' not specified')
            process.exit(1)
        }
        args[val.replace(/-+/g, '')] = process.argv[argIdx + 1]
    })
    return args
}

const args = parseArgs(['--input', '--seed'])

const kp = Stellar.Keypair.fromSecret(args.seed)
const sig = kp.sign(args.input)
const data = Buffer.from(sig).toString('base64')
console.log(data)
