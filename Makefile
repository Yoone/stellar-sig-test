# Golang
GO = go
PACKAGE = stellar-sig-test
BINDIR = $(CURDIR)/bin
OUT = $(BINDIR)/$(PACKAGE)

GOPATH = $(CURDIR)/.gopath
GOBIN = $(GOPATH)/bin
GOSRC = $(GOPATH)/src
BASE = $(GOSRC)/$(PACKAGE)

GODEP = $(GOBIN)/dep

# Python
ACTIVATE = virtualenv/bin/activate

# Stellar
MESSAGE = OXIO[dot]io
SEED = SBOXR2R5KDBOBP6K3T35OXN3ELOO7BBDOTHWYPO7IGEHJOQK5RU7VLLH

export GOPATH
export PATH := $(PATH):$(GOBIN)

.PHONY: all
all: vendor build pip-update node-update

##
# Project
##

$(BASE):
	mkdir -p $(dir $@)
	ln -sf $(CURDIR)/src $@

.PHONY: build
build: | $(BASE)
	cd $(BASE) && $(GO) build -o $(OUT) main.go

virtualenv:
	virtualenv --no-site-packages --distribute -p $(shell which python3) $@

.PHONY: run
run:
	@echo "--- Golang ---"
	@$(OUT) -input $(MESSAGE) -seed $(SEED)
	@echo "--- Python ---"
	@source $(ACTIVATE) && ./src/main.py --input $(MESSAGE) --seed $(SEED)
	@echo "--- JavaScript ---"
	@node --harmony ./src/main.js --input $(MESSAGE) --seed $(SEED)

##
# Dependencies management
##

$(BASE)/Gopkg.toml: | $(BASE) $(GODEP)
	if [ ! -f $(BASE)/Gopkg.toml ]; then cd $(BASE) && $(GODEP) init; fi

.PHONY: vendor
FLAGS=
vendor: | $(BASE)/Gopkg.toml
	cd $(BASE) && $(GODEP) ensure -v $(FLAGS)

.PHONY: pip-update
pip-update: virtualenv
	source $(ACTIVATE) && \
	pip3 install --upgrade pip && \
	pip3 install -r requirements.txt

.PHONY: node-update
node-update:
	npm install

##
# Tools
##

$(GOBIN):
	mkdir -p $@

$(GOBIN)/%: | $(BASE) $(GOBIN)
	$(GO) get $(REPOSITORY)

$(GODEP): REPOSITORY=github.com/golang/dep/cmd/dep

##
# Misc
##

.PHONY: clean
clean:
	$(RM) $(OUT)

.PHONY: distclean
distclean: clean
	$(RM) -r src/vendor $(BINDIR) $(GOPATH)
	$(RM) -r virtualenv
	$(RM) -r node_modules
